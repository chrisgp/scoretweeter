package com.juggledmedia.ScoreTweeter;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

public class ScoreTweeterActivity extends Activity {
	public static final int RED = 1;
	public static final int BLUE = 0;
	public static final String SCORE_FILE = "ScoreFile";
	static SharedPreferences storedScores;
	static SharedPreferences.Editor editor;
	int leftScore=0;
	int rightScore=0;
	String leftName="";
	String rightName="";
	TextView rightScoreText=null;
	TextView leftScoreText=null;
	EditText editTextLeft;
	EditText editTextRight;
	Button rightColor, leftColor;
	LinearLayout rightPanel, leftPanel;
	int rightPanelColor = 1;
	int leftPanelColor = 0;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main);

		leftScoreText = (TextView) findViewById(R.id.left_score);
		rightScoreText = (TextView) findViewById(R.id.right_score);

		editTextLeft = (EditText) findViewById(R.id.team_name_left);
		editTextRight = (EditText) findViewById(R.id.team_name_right);
		
		rightColor = (Button) findViewById(R.id.right_color);
		leftColor = (Button) findViewById(R.id.left_color);
		
		rightPanel = (LinearLayout) findViewById(R.id.right_panel);
		leftPanel = (LinearLayout) findViewById(R.id.left_panel);
		
		storedScores = getSharedPreferences(SCORE_FILE, 0);
		editor = storedScores.edit();
	    leftScore = storedScores.getInt("left",0);
	    rightScore = storedScores.getInt("right",0);
	    leftName = storedScores.getString("leftName", "");
	    rightName = storedScores.getString("rightName","");
	    
	    editTextLeft.setText(leftName);
	    editTextRight.setText(rightName);
	    
	    changeScoreTexts();

	}

	public void sendTweet(View view){
		leftName=editTextLeft.getText().toString();
		rightName=editTextRight.getText().toString();
		String tweetUrl = "https://twitter.com/intent/tweet?text="+leftName+" "+leftScore+"-"+rightScore+" "+rightName;
		Uri uri = Uri.parse(tweetUrl);
		startActivity(new Intent(Intent.ACTION_VIEW, uri));
	}
	public void reset(View view){
		leftScore=0;
		rightScore=0;
		changeScoreTexts();
	}
	public void leftUp(View view){
		leftScore++;
		changeScoreTexts();
	}
	public void leftDown(View view){
		if(leftScore>0){
			leftScore--;
			changeScoreTexts();
		}
	}
	public void rightUp(View view){
		rightScore++;
		changeScoreTexts();
	}
	public void rightDown(View view){
		if(rightScore>0){
			rightScore--;
			changeScoreTexts();
		}
	}
	public void changeRightColor(View view){
		rightPanelColor = (rightPanelColor+1)%2;
		if(rightPanelColor==1){
			rightPanel.setBackgroundColor(Color.RED);
			rightColor.setText("Red");
		}
		else{
			rightPanel.setBackgroundColor(Color.BLUE);
			rightColor.setText("Blue");
		}
	}
	public void changeLeftColor(View view){
		leftPanelColor = (leftPanelColor+1)%2;
		if(leftPanelColor==1){
			leftPanel.setBackgroundColor(Color.RED);
			leftColor.setText("Red");
		}
		else{
			leftPanel.setBackgroundColor(Color.BLUE);
			leftColor.setText("Blue");
		}
	}
	public void changeScoreTexts(){
		leftScoreText.setText(leftScore+"");
		rightScoreText.setText(rightScore+"");
	}
	@Override
	protected void onPause(){
		editor.putInt("left", leftScore);
		editor.putInt("right", rightScore);
		editor.putString("leftName", editTextLeft.getText().toString());
		editor.putString("rightName", editTextRight.getText().toString());
		editor.commit();
		super.onPause();
	}
}