ScoreTweeter
============

A very simple app to keep a scoreboard of a game and then easily tweet the score.  

Available on the Google Play store: 
https://play.google.com/store/apps/details?id=com.juggledmedia.ScoreTweeter&hl=en
